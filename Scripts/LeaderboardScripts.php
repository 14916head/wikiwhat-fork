<?php

$host = "wikiwhatdb.comkxvb009zg.eu-west-2.rds.amazonaws.com";
$username = "root";
$password = "eatsshootsandleaves";
$db_name = "server-article-data";

$conn = mysqli_connect($host, $username, $password, $db_name);

if(isset($_POST['category'])) {
$category = $_POST['category'];
} else {
  $category = "AllTime";
}

if (mysqli_connect_error()){
  die('connect error(' . mysqli_connect_errno() . ')' . mysqli_connect_error());
}
else {
  //   // SELECT CATEGORIES
  try {
    if($category == "AllTime") {
      $sql_top_scores = "SELECT position, username, score FROM tbl_top_scores ORDER BY position;";
    } else {
      $sql_top_scores = "SELECT position, username, score FROM tbl_top_scores WHERE category_ID =".$category." ORDER BY position;";
    }
    
    $top_scores = mysqli_query($conn, $sql_top_scores);
    $rows = [];
    
    while($row = mysqli_fetch_assoc($top_scores)){ // for each category output radio button
      array_push($rows, $row);
    }
  }
  catch (\Throwable $th) {
    echo("SQL Error " . $th);
  }
}


?>

<script>
let category = <?php echo(json_encode($category)); ?>;

function getScores(){
    //Retrieves Scores from Database
    
    return <?php echo(json_encode($rows)); ?>;
}


function generateTableHead(table, data) {
    let thead = table.createTHead();
    let row = thead.insertRow();
    for (let key of data) {
        let th = document.createElement("th");
        let text = document.createTextNode(key);
        th.appendChild(text);
        row.appendChild(th);
  }
}

function generateTableData(table, data) {
    for (let element of data) {
        let row = table.insertRow();
        for (key in element) {
            let cell = row.insertCell();
            let text = document.createTextNode(element[key]);
            cell.appendChild(text);
      }
    }
  }

function generateTable() {
    let table = document.querySelector("table");
    let data = getScores();
    let data_keys = Object.keys(data[0]);
    document.getElementById("Leaderboard-data").innerHTML = category;

    generateTableHead(table, data_keys);
    generateTableData(table,data);
}

function generateButtons(numofcategories) {
    Board = document.getElementById("Leaderboard-buttons")

    Alltime_button = document.createElement("button");
    Alltime_button.onClick = buttonClick('All Time');
    Alltime_button.innerText = "All Time" + numofcategories;

    Board.appendChild(Alltime_button);
}

function displayBoardType(category) {
    document.getElementById("Leaderboard-data").innerHTML = category;
}

// function buttonClick(newcategory) {
//     category = newcategory;
    // displayBoardType(category);
// }
</script>