<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>WikiWhat - Game</title>

    <link rel="stylesheet" type="text/css" href="Stylesheets/WikiWhatStyles.css">
    <link rel="stylesheet" type="text/css" href="Stylesheets/BorderStyles.css">

    <script src="Scripts/WikiWhatScripts.js"></script>
    
    <?php
    include("Scripts/GameScripts.php");
    ?>
</head>

<body onload="OnloadedFuncs();">

<div id="grid">

    <div id="logo-container">
        <img src="logo.jpg" width="100" alt="My Image" class="logo"/>
    </div>


    <div class="page-header">
        <div id="article-tab"></div>
       
        <button class="profile-button" onclick="window.location.href='Profile.php'">Profile</button>
    </div>


    <div class="sidenav">
        <button class="home-button" onclick="window.location.href='Home.html'">Homepage</button>
        <button class="lb-button" onclick="window.location.href='Leaderboard.php'">Leaderboard</button>
        <div class="license">
          <h4>WikiWhat</h4>
          <a href="LICENSE.txt"
            ><span class="small">Release under MIT License</span></a
          >
        </div>
    </div>


    <div id="article-content">
    
        <div id="score-display">
            Score
        </div>
        
        <?php 
            $category_name = $_SESSION['category_name'];
        ?>
        
        <form id="answer-form">
            <input type="text" onkeypress="return event.keyCode != 13" name="guess" id="guess-input-box"
            placeholder="Article Title..."/> 
            <button name="enter-button" type="button" id="text-box-button" onclick="UsersAttempt();">Enter</button>
            <input type="submit>
        </form>
        <hr>
    
    </div>

</div>

</body>
</html>