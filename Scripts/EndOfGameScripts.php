<?php
  
  $host = "wikiwhatdb.comkxvb009zg.eu-west-2.rds.amazonaws.com";
  $username = "root";
  $password = "eatsshootsandleaves";
  $db_name = "test-score-data";
  
  $conn = mysqli_connect($host, $username, $password, $db_name);
  
  session_start();
  
  $category_ID = $_SESSION['categoryID'];
  if (mysqli_connect_error()){
    die('connect error(' . mysqli_connect_errno() . ')' . mysqli_connect_error() . "<br>");
  }
  else {
    //   // SELECT CATEGORIES
    try {
      $sql_top_scores = "SELECT position, username, score FROM tbl_top_scores WHERE category_ID =".$category_ID." ORDER BY position;";
      $top_scores = mysqli_query($conn, $sql_top_scores);
      $rows = [];
      
        while($row = mysqli_fetch_assoc($top_scores)){ // for each category output radio button
        array_push($rows, $row);
      }
    
    }
    catch (\Throwable $th) {
      echo("SQL Error " . $th . "<br>");
    }
  }



$posted_score = false;

if(isset($_POST['requested-upload-score'])) {
  echo("requested-upload-score is set" . "<br>");
  $posted_score = true;
  $is_top_score = false;
  $position = 2;

  foreach ($rows as $row) {
    echo("score to beat: " . $row['score']);
    echo("score " . $_POST['score']);
    echo($_POST['score'] > $row['score'] . "<br>");

    if($_POST['score'] > $row['score']) {
      $position = $row['position'];
      $sql_move_positions_down = "UPDATE tbl_top_scores
                                  SET position = position + 1
                                  WHERE position >= ".$row['position'].";";
      $is_top_score = true;
      break;
    }

    echo(".  ");
  }

  if($is_top_score) {
    $move_positions_down = mysqli_query($conn, $sql_move_positions_down);
  }

  $sql_submit_score = ("INSERT INTO tbl_top_scores
                        (position, username, score, category_ID) VALUES ("
                        . $position . ", '" . $_SESSION['username'] . "', "
                        . $_POST['score'] . ", ". $category_ID . ");");
  echo("name: ".$_SESSION['username']. "<br>");
  echo("position: ".$position. "<br>");
  echo("cat_ID: ".$category_ID. "<br>");
  try {
    $submit_score = mysqli_query($conn, $sql_submit_score);
  } catch (\Throwable $th) {
    echo("Error trying " . $sql_submit_score . "<br>" . $th . " Err no:" . mysqli_error($conn) . "<br>");
  }
}


?>

<script>

let leaderboard = <?php echo(json_encode($rows)); ?>;
let posted_score = <?php echo(json_encode($posted_score)); ?>;


function getScores(){
    //Retrieves Scores from Database
    return leaderboard;
}

function generateTableHead(table, data) {
    let thead = table.createTHead();
    let row = thead.insertRow();
    for (let key of data) {
        let th = document.createElement("th");
        let text = document.createTextNode(key);
        th.appendChild(text);
        row.appendChild(th);
  }
}

function generateTableData(table, data) {
    for (let element of data) {
        let row = table.insertRow();
        for (key in element) {
            let cell = row.insertCell();
            let text = document.createTextNode(element[key]);
            cell.appendChild(text);
      }
    }
  }

function generateTable() {
    let table = document.querySelector("table");
    let data = getScores();
    let data_keys = Object.keys(data[0]);
    /* document.getElementById("Leaderboard-data").innerHTML = category; */

    generateTableHead(table, data_keys);
    generateTableData(table,data);
}

function LoadNewGame() {
  location.replace("Home.html");
}

function LoadEndTitle() {
  /*Displayed the function after every round (aside from the last round)*/
  let Vals = window.sessionStorage.getItem("EndScreenVals");
  Vals = JSON.parse(Vals);
  console.log("Vals: " + Vals);
  let List_of_Text = Vals[0];
  let score = Vals[1];
  let round = Vals[2];
  let name = Vals[3];

  page_content = document.getElementById("reset-page");
  end_title = document.createElement("div");

  for (let i = 0; i < 4; i++) {
    let j = i + 1;
    let header = document.createElement("h" + j);
    header.innerText = List_of_Text[i];
    header.setAttribute("id", "centre-text");
    end_title.appendChild(header);
  }
  page_content.appendChild(end_title);

  if (round < 6) {
    end_butt_title = document.createElement("div");
    end_butt_title.setAttribute("id", "button-styling");

    let nextround_button = document.createElement("button");

    nextround_button.innerText = "Next Round";
    nextround_button.onclick = LoadNewRound;

    nextround_button.setAttribute("class", "nextround-button");

    end_butt_title.appendChild(nextround_button);
    page_content.appendChild(end_butt_title);
  } else {

    if(!posted_score) {
      //generateTable();
      submit_score_form = document.createElement("form");
      submit_score_form.setAttribute("action", "EndofGame.php");
      submit_score_form.setAttribute("method", "POST");

      let scorePoster = document.createElement("input");
      scorePoster.type = "text";
      scorePoster.readOnly = true;
      scorePoster.name = "score";
      scorePoster.value = score;
      submit_score_form.appendChild(scorePoster);

      hidden_checkbox = document.createElement("input");
      hidden_checkbox.setAttribute("type", "checkbox");
      hidden_checkbox.checked = true;
      hidden_checkbox.setAttribute("name", "requested-upload-score");
      hidden_checkbox.setAttribute("hidden", true);
      submit_score_form.appendChild(hidden_checkbox);

      submit_score_button = document.createElement("input");
      submit_score_button.setAttribute("type", "submit");
      submit_score_button.setAttribute("value", "submit score");
      submit_score_form.appendChild(submit_score_button);

      page_content.appendChild(submit_score_form);
    }

    end_butt_title = document.createElement("div");
    end_butt_title.setAttribute("id", "button-styling");

    if(posted_score) {
      sessionStorage.removeItem("EndScreenVals");
    }

    let newgame_button = document.createElement("button");
    newgame_button.innerText = "New Game";
    newgame_button.onclick = LoadNewGame;
    newgame_button.setAttribute("class", "newgame-button");

    end_butt_title.appendChild(newgame_button);
    page_content.appendChild(end_butt_title);
    window.sessionStorage.clear();
  }

  // Note: LoadNewRound has been moved to Scripts/WikiWhatScripts.js
}



</script>
