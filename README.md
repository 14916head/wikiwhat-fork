<img src="logo.jpg" width="100" alt="Logo">

# WikiWhat Fork


## Behind the project

This is a fork of an MIT-licensed team project I (Maarij Khan - the owner of this repo) was a part of. This is a redeployment of it using AWS so it is no longer dependent on the University of Manchester's phpMyAdmin services.

<img src="CategoriesPageScreenshot.png" width="600" alt="Screenshot of Categories.php">

<!-- ## Name
Choose a self-explaining name for your project. -->

## Description
This is WikiWhat, a game where you guess a randomly selected Wikipedia article given only the first 5 hyperlinks on the page. Try to get onto the leaderboard (or not, it's up to you!), and interact with the vast Wikipedia archives in a new way.

## How to play
Currently there is no provided external server, so running a localhost server will be required.

If wanting to add wikipedia pages to the current database, uncommenting `;extension=curl` to `extension=curl` in the php.ini file to allow curl functionality, and installing "https://curl.haxx.se/ca/cacert.pem" and  uncommenting  `;curl.cainfo =` to `curl.cainfo = "...\cacert.pem"` in the php.ini file where `...` is the absolute path of the "cacert.pem" file, so the certificate gets recognised.

## Authors and acknowledgment
This fork has been worked on by:
 - Maarij Khan

The source of this project has been worked on by:
 - Lucia Wright
 - Na Wang
 - Charlotte Wilkinson
 - Siddharth Rathod
 - Maarij Khan
 - Yuxiao Zhao

## License
This is MIT licensed, as is the source of the fork.

<!-- ## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers. -->

