<!DOCTYPE html>
<html>
<head>
<title>Wiki Fetcher</title>
</head>
<body>
    
<?php

function wikiRequest($pages){
    //takes an array of page names
    //returns an array of page files as strings
    //creates the curl object with required options
    //loops through each page in the parameter array and reasigns the URL and re-executes the curl object for each one, collecting the responses in an array
    $curl=curl_init();
    $responses=array();
    curl_setopt($curl,CURLOPT_USERAGENT,"Student Project Contact: lucia.wright@student.manchester.ac.uk");
    curl_setopt($curl,CURLOPT_CUSTOMREQUEST,"GET");
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
    foreach ($pages as $pTitle){
        $URL="https://en.wikipedia.org/w/api.php?action=parse&page=".$pTitle."&format=json";
        curl_setopt($curl,CURLOPT_URL,$URL);
        array_push($responses,array(curl_exec($curl),$pTitle));
        
        if(curl_error($curl)) {
            echo(curl_error($curl) + " ");
        }
        $err=curl_errno($curl);
        if($err){
            die("Curl Error: ". $err);
        }
    }
    return($responses);
}
function validate($link,$pTitle){
    //takes a link and the page title it's from
    //returns boolean of validity
    
    $valid = true;
    $link=strtolower($link);
    $pTitle=strtolower($pTitle);
    $bannedWords=explode("_",$pTitle);
    array_push($bannedWords,"\u");
    $allowed_matches=array("the","so","and","an","a","of","to","in");
    $invalid_matches=array("edit","verification needed","citation needed","failed verification","unreliable source?","citation not found","listen");
    
    if (strlen($link)<2 or strlen($link)>50){$valid=false;}
    else if (in_array($link,$invalid_matches)){$valid=false;}
    else if ($link[0]=="&"){$valid=false;}
    else if (strlen($link)>6 and is_numeric(str_replace("-","",$link))){$valid=false;}
    else{
        foreach ($bannedWords as $word){
            if (in_array($word,$allowed_matches)==false and strstr($link,$word)!=false){
                $valid=false;
            }
        }
    }
    
    return($valid);
}

function extractLinks($page){
    //takes a long page string
    //returns an array with first item array of links, second item page name string
    //removes irrelevant tags, cuts off anything after the references section,
    //and anything before the first paragraph tag (to remove navigation sections and disambiguations of the same page)
    //removes the initial paragraph tag, then splits the text using the a tag
    //split sections are looped through to extract the text of the link from between the closing of the <a> tag to the </a> tag
    //validates the extracted links and puts them in an array

    $allowed_tags=array('<a>','<p>','</a>');
    $response=ltrim(strchr(strchr(strip_tags($page[0],$allowed_tags),"References",True),"<p>"),"<p>");
    $split_response=explode("<p>",$response);
    $final_links=array();

    foreach ($split_response as $paragraph) {
    	$links=explode("<a",$paragraph);

    	foreach ($links as $link){
    		$link=strchr($link,"</a>",True);
    		$link=trim(ltrim(strchr($link,">"),">"));
    		$link=str_replace("\\\"",'"',$link);
    		$link = str_replace("'","",$link);

    		if (validate($link,$page[1])==true){
    			array_push($final_links,$link);    			
    		}
    	}
    }
    return(array(array_unique($final_links),$page[1]));
}



function addPagesAndHints($array_of_pages, $array_of_hints,$category_ID,$conn) {
    //takes an array of page names as strings, an array of hints as strings, a category id as an integer, and a connection object for sql
    //returns nothing
    //inserts the page into the pages table and the hints into the hints table
	
	$i = 0;

	foreach($array_of_pages as $page_name) {	    
	    $page_url = ("https://en.wikipedia.org/wiki/".$page_name);	    
	    $page_name = str_replace("_"," ",$page_name);
	    
	    $sql_add_page = ("INSERT INTO tbl_pages (ID, category_ID, page_name, page_url) VALUES (NULL, ".$category_ID.", '".$page_name."', '".$page_url."');");
	    
	    
	    if (!$conn){            
            die('connect error('.mysqli_connect_errno().')'.mysqli_connect_error());
	    }
	    else{
	        $result_add_page = mysqli_query($conn, $sql_add_page);
        }	    
	    
	    $sql_get_page_ID = ("SELECT ID FROM `tbl_pages` WHERE page_name = '".$page_name."';");
	    
	    $result_get_page_ID = mysqli_query($conn, $sql_get_page_ID);
	    $sql_result_array = mysqli_fetch_array($result_get_page_ID);
	    $page_ID = $sql_result_array[0];	    
	    
	    foreach($array_of_hints[$i] as $page_hint){	        
            $sql_add_hint = ("INSERT INTO tbl_hints (ID, page_ID, page_hint) VALUES (NULL, ".$page_ID.", '".$page_hint."');");                
            $result_add_hint = mysqli_query($conn, $sql_add_hint);	        
	    }
        
	    $i++;
	    
	}
}




$host = "wikiwhatdb.comkxvb009zg.eu-west-2.rds.amazonaws.com";
$username = "root";
$password = "eatsshootsandleaves";
$db_name = "server-article-data";

$conn = mysqli_connect($host, $username, $password, $db_name);

if (!$conn) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
        . mysqli_connect_error());
}

else if (isset($_POST['page']) and isset($_POST['cat_ID'])){
    $category_ID = intval($_POST['cat_ID']);
    $page_name = str_replace(" ","_",$_POST['page']);
    
    $pages=array($page_name);
    $responses=wikiRequest($pages);
    $all_hints=array();
    
    foreach($responses as $response) {
        $links=extractLinks($response);
        $hints=array_slice($links[0],0,5);
        array_push($all_hints,$hints);
        
    }

    addPagesAndHints($pages, $all_hints,$category_ID,$conn);
    
}
else {
    echo ("No pages selected");
}

$_POST['cat_ID']=NULL;
$_POST['page']=NULL;
?>
<form method="post" action="APILinksFetching.php">
    <label for="page">Page: </label><br>
    <input type="text" id="page" name="page"><br>
    <label for="cat_ID">Category ID: </label><br>
    <input type="text" id="cat_ID" name="cat_ID"><br>
    <input type="submit" value="Submit"><br>
</form>
<br>Script Complete.
</body>
</html>
