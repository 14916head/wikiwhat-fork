function LoadNewRound() {
  location.replace("Game.php");
}

function GenerateLinesOfText(lines_container, GenerateLine) {
  /* Parameter/s:	
		- lines_container : HTML element
		  - The element that all the lines will be made children of

	    - GenerateLine : Function
		  - The function called when each line is being generated, that puts the
		 	stuff that will be in each line in it. Gets called every line and is
		 	passed the line number currently being generated when called.
	*/

  for (var j = 0; j < 3; j++) {
    paragraph_line = null;
    paragraph_line = document.createElement("div");
    lines_container.appendChild(paragraph_line);

    paragraph_line.setAttribute("style", "display: flex;");
    paragraph_line.style.margin = 0;
    paragraph_line.setAttribute("class", "line-of-text");

    GenerateLine(j);
  }
}