<?php 

function updateScore()
{
  session_start();
  if(!isset($_SESSION['score'])) {
    $_SESSION['score'] = 0;
  }
  $_SESSION['score'] += (5 - $incorrect_answers) * 10;
  return ["inc ans",$incorrect_answers,"score",$_SESSION['score']];
}

function incrementIncorrectAnswers()
{
  $incorrect_answers = $incorrect_answers + 1;
  return $incorrect_answers;
}

session_start();

if (isset($_POST['categoryID'])) { 
  $_SESSION['categoryID'] = $_POST['categoryID'];
}
$category_ID = $_SESSION['categoryID'];

$host = "wikiwhatdb.comkxvb009zg.eu-west-2.rds.amazonaws.com";
$username = "root";
$password = "eatsshootsandleaves";
$db_name = "server-article-data";

$conn = mysqli_connect($host, $username, $password, $db_name);
if (mysqli_connect_error()){
  die('connect error('.mysqli_connect_errno().')'.mysqli_connect_error());
}
else {

	$sql_articles_query = ("SELECT * FROM tbl_pages WHERE category_ID = ".$category_ID);
	$articles = mysqli_query($conn, $sql_articles_query);
	$list_of_hints = array();

  $i = 0;
  $list_of_hints = array();
  $list_ofanswers = array();
	while($row = mysqli_fetch_assoc($articles)) {
    $article_ID = $row['ID'];

    $sql_for_hints = ("SELECT * FROM tbl_hints WHERE page_ID = ".$article_ID);
    $sql_articles_query = ("SELECT * FROM tbl_pages WHERE category_ID = ".$category_ID);
    $loop_of_hints = mysqli_query($conn, $sql_for_hints);
        
    $list_of_hints[$i] = array();
    $list_of_answers[$i] = $row['page_name'];

		$j = 0;
		while($row2 = mysqli_fetch_assoc($loop_of_hints)) {
      $list_of_hints[$i][$j] = $row2['page_hint'];
      $j = $j + 1;
		}
		
		$i = $i + 1;
	}
}
?>

<?php $incorrect_answers = 0; ?>

<script>

//Using MVC architecture (sorting code into data storing, getting/sorting data, outputting data)
//=======================Model==================================//
let hints = <?php echo(json_encode($list_of_hints)); ?>;
// let hints = [];
let answers = <?php echo(json_encode($list_of_answers)); ?>;
let randomised = false;
let name = <?php echo(json_encode($_SESSION['username'])); ?>;

let incorrect_answers = 0;
let round = 1;
const score_multiplier = 10;
let score = 0;

function GenerateAnswer() {
  for (let i=answers.length-1;i>=0;i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var temp = answers[i];
    answers[i] = answers[j];
    answers[j] = temp;

    var temp = hints[i];
    hints[i] = hints[j];
    hints[j] = temp;
  }
  for (let i=0;i<answers.length;i++) {
    for (let k=0;k<5;k++) {
      hints[i][k] += "~~";
    }
    answers[i] += "~~";
  }

  sessionStorage.setItem("Answers",answers);
  sessionStorage.setItem("Hints",hints);
}

//========================View====================================
document.onkeyup = function (e) {
  e = e || window.event;
  switch (e.which || e.keyCode) {
    case 13 : UsersAttempt()
      break;
  }
}

function UsersAttempt() {
  /*Validates User's Answer*/
  const entry_form = document.getElementById("guess-input-box");
  let user_guess = entry_form.value;
  if (CheckAnswer(user_guess)) {
    EndofGameDisplay(true);
  } else {
    IncorrectAnsDisplay();
  }
}

function CheckAnswer(user_guess) {
  /*Checks if User's Answer is the same as actual answer*/
  // Validate capitalisation of a word; omitting one letter from a word
  let lengths = user_guess.length;
  let index = Math.random() * (lengths - 0) + 0;
  let guess_random = user_guess[index];
  if (
    user_guess.toUpperCase() === answers[round-1].toUpperCase() ||
    user_guess.toUpperCase() === answers[round-1].toUpperCase().replace(guess_random, "")
  ) {
    return true;
  } else {
    return false;
  }
}

function CalculateStats(incorrect_answers, round, score) {
  /*Calculates score and round number*/
  score = score + (5 - incorrect_answers) * score_multiplier;
  round+=1;
  return [round, score];
}
//=======================Controller================================

function OnloadedFuncs() {
  /*Functions that will run when page is initially opened*/
  
  if (sessionStorage.getItem("EndScreenVals") != null) {
    let Vals = window.sessionStorage.getItem("EndScreenVals");
    Vals = JSON.parse(Vals);
    score = Vals[1];
    round = Vals[2];
  }

  if (round == 1) {
    GenerateAnswer();
  }

  answers = sessionStorage.getItem("Answers").split("~~,");
  finalans = answers[answers.length-1];
  answers[answers.length-1] = finalans.substring(0,finalans.length-2); 

  hintsinonearray = sessionStorage.getItem("Hints").split("~~,");
  finalhint = hintsinonearray[hintsinonearray.length-1];
  hintsinonearray[hintsinonearray.length-1] = finalhint.substring(0,finalhint.length-2);

  hints = new Array(answers.length);
  let hintrow = [];
  for (var i=0;i<answers.length;i++) {
    hints[i] = new Array(5);
    for (var j=0;j<5;j++) {
      hints[i][j] = hintsinonearray[(i*5)+j];
    }
  }

  sessionStorage.clear();
  DisplayNewHint(0);
  DisplayRound();
}

function GenerateGameLine(line_index) {
  for (var k = 0; k < 3 + Math.floor(Math.random() * 4); k++) {
    if (line_index == 1 && k == 1) {
      let hint = hints[round - 1][incorrect_answers];

      let link = null;
      link = document.createElement("a");
      paragraph_line.appendChild(link);
      link.setAttribute("href", hint);
      link.innerText = hint;
    } else {
      let chosen_word = null;
      chosen_word = document.createElement("div");
      paragraph_line.appendChild(chosen_word);
      chosen_word.setAttribute("class", "content-text-word");
      let length = 1 + Math.floor(Math.random() * 4);
      chosen_word.setAttribute("style", "flex:" + length + ";");
    }
  }
}

function DisplayRound() {
  /*Displays the round number*/
  round_paragraph = document.getElementById("score-display");

  round_paragraph.innerHTML = "";
  round_text = document.createElement("div");
  round_text.id = "round-text";
  round_text.innerText = "Round: " + round;

  round_paragraph.appendChild(round_text);
}

function DisplayNewHint(Index) {
  /*Takes the next hint in the list of hints and displays it on screen*/
  let hint = hints[round - 1][Index];

  article_content = document.getElementById("article-content");
  main_paragraph = null;
  main_paragraph = document.createElement("div");
  article_content.appendChild(main_paragraph);
  main_paragraph.setAttribute("class", "main-paragraph");

  GenerateLinesOfText(main_paragraph, GenerateGameLine);
}

function IncorrectAnsDisplay() {
  /*Continues the game until 5 incorrect answers are enterred*/
  incorrect_answers++;
  if (incorrect_answers < 5) {
    DisplayNewHint(incorrect_answers);
  } else {
    EndofGameDisplay(false);
  }
}


function EndofGameDisplay(ans) {
  /*Displays stats at the end of the game*/
  [round, score] = CalculateStats(incorrect_answers, round, score);
  let end_phrase = "";

  var List_of_Text = ["End of Round " + (round - 1), end_phrase, "Score: " + score, name];

  if (round === 6) {
    List_of_Text[0] = "End of Game";
  }

  window.sessionStorage.setItem(
    "EndScreenVals",
    JSON.stringify([List_of_Text, score, round, name])
  );

  let newhints = hints;
  let newanswers = answers;
   for (let i=0;i<newanswers.length;i++) {
    for (let k=0;k<5;k++) {
      newhints[i][k] += "~~";
    }
    newanswers[i] += "~~";
  }
  sessionStorage.setItem("Answers",newanswers);
  sessionStorage.setItem("Hints",newhints);
  
  location.replace("EndofGame.php");
}

</script>