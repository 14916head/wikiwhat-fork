<!DOCTYPE html>

<?php

$host = "wikiwhatdb.comkxvb009zg.eu-west-2.rds.amazonaws.com";
$username = "root";
$password = "eatsshootsandleaves";
$db_name = "server-article-data";
$port = "3306";

$conn = mysqli_connect($host, $username, $password, $db_name, $port);

if (mysqli_connect_error()){
  die('connect error('.mysqli_connect_errno().')'.mysqli_error($conn));
}

?>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>WikiWhat - Categories</title>

    <link
      rel="stylesheet"
      type="text/css"
      href="Stylesheets/WikiWhatStyles.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="Stylesheets/BorderStyles.css"
    />

    <script src="Scripts/CategoriesScripts.js" type="text/javascript"></script>
  </head>

  <body>
  <div id="grid">
      <div id="logo-container">
        <img src="logo.jpg" width="100" alt="My Image" class="logo" />
      </div>

      <div class="page-header">
        
        <button
          class="profile-button"
          onclick="window.location.href='Profile.php'"
        >
          Profile
        </button>
      </div>

      <div class="sidenav">
        <button class="home-button" onclick="window.location.href='Home.html'">
          Homepage
        </button>
        <button
          class="lb-button"
          onclick="window.location.href='Leaderboard.php'"
        >
          Leaderboard
        </button>
        <div class="license">
          <h4>WikiWhat</h4>
          <a href="LICENSE.txt"
            ><span class="small">Release under MIT License</span></a
          >
        </div>
      </div>


    <div class="category-container">
      <form method="POST" action="Game.php">
      <?php

        $sql_categories = "SELECT * FROM tbl_categories;";
        $result_categories = mysqli_query($conn, $sql_categories);

        while($row = mysqli_fetch_assoc($result_categories)){ // for each category

          echo('<div class="card">');
          echo('<div class="card-details">');
          echo('<p class="text-title">'.$row['category_name'].'</p>');
          echo('</div>');
          echo('<button class="card-button" type="submit" name="categoryID" value="'.$row['ID'].'">');

          echo('Play');
          echo('</button></div>');

        }

      ?>
      </form>
    </div>  

  </body>
</html>
