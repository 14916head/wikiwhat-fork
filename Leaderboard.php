<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>WikiWhat - Leaderboards</title>

    <link rel="stylesheet" type="text/css" href="Stylesheets/WikiWhatStyles.css">
    <link rel="stylesheet" type="text/css" href="Stylesheets/BorderStyles.css">
    <link rel="stylesheet" type="text/css" href="Stylesheets/LeaderboardStyles.css">

    <?php
    include("Scripts/LeaderboardScripts.php");
    ?>
</head>

<body onload="generateTable();">
    <div id="grid">
        <div id="logo-container">
            <img src="logo.jpg" width="100" alt="My Image" class="logo" />
        </div>
        <div class="page-header">
            
            <button class="profile-button" onclick="window.location.href='Profile.php'">
              Profile
            </button>
        </div>

        <div class="sidenav">
        <button class="home-button" onclick="window.location.href='Home.html'">
            Homepage
        </button>
        <button class="lb-button" onclick="window.location.href='Leaderboard.php'">
            Leaderboard
        </button>
        <form action="#" method="POST">
            <hr>
            <button name="category" type="submit" value="AllTime" onClick="buttonClick('All Time')">
                All Time
            </button>

            <button name="category" type="submit" value="1" onClick="buttonClick('Category 1')">
                Category 1
            </button>

            <button name="category" type="submit" value="2" onClick="buttonClick('Category 2')">
                Category 2
            </button>

            <button name="category" type="submit" value="3" onClick="buttonClick('Category 3')">
                Category 3
            </button>

            <button name="category" type="submit" value="4" onClick="buttonClick('Category 4')">
                Category 4
            </button>
        </form>
        <div class="license">
      <h4>WikiWhat</h4>
      <a href="LICENSE.txt"
        ><span class="small">Release under MIT License</span></a
      >
    </div>
</div>

    <div class="leaderboard">
        <h1 id="leaderboard-title">
            Leaderboard
        </h1>
        <h2 id="Leaderboard-data">
            <!--Displays current leaderboard type-->
        </h2>
        <table>
        </table>
    </div>


</body>
</html>
