<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>WikiWhat</title>

    <link rel="stylesheet" type="text/css" href="Stylesheets/WikiWhatStyles.css">
    <link rel="stylesheet" type="text/css" href="Stylesheets/BorderStyles.css">

    <script src="Scripts/WikiWhatScripts.js"></script>
    <?php
    include("Scripts/EndOfGameScripts.php");
    ?>
</head>

<body onload="LoadEndTitle();">
    <div id="grid">
        <div id="logo-container">
          <img src="logo.jpg" width="100" alt="My Image" class="logo" />
        </div>
  
        <div class="page-header">
          
          <button
            class="profile-button"
            onclick="window.location.href='Profile.php'"
          >
            Profile
          </button>
        </div>
  
        <div class="sidenav">
          <button class="home-button" onclick="window.location.href='Home.html'">
            Homepage
          </button>
          <button
            class="lb-button"
            onclick="window.location.href='Leaderboard.php'"
          >
            Leaderboard
          </button>
          <div class="license">
          <h4>WikiWhat</h4>
          <a href="LICENSE.txt"
            ><span class="small">Release under MIT License</span></a
          >
        </div>
        </div>

        <div id="reset-page" style="padding-left:30%;">
          <table></table>
        </div>
</body>
</html>