<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>WikiWhat - Profile</title>

    <link
      rel="stylesheet"
      type="text/css"
      href="Stylesheets/WikiWhatStyles.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="Stylesheets/BorderStyles.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="Stylesheets/ProfileStyles.css"
    />
  </head>

  <body>
    <div id="grid">
      <div id="logo-container">
        <img src="logo.jpg" width="100" alt="My Image" class="logo" />
      </div>
      <div class="page-header">
        <?php
        session_start();
      
        ?>
        
        <button
          class="profile-button"
          onclick="window.location.href='Profile.php'"
        >
          Profile
        </button>
      </div>

      <div class="sidenav">
        <button class="home-button" onclick="window.location.href='Home.html'">
          Homepage
        </button>
        <button
          class="lb-button"
          onclick="window.location.href='Leaderboard.php'"
        >
          Leaderboard
        </button>
        <div class="license">
          <h4>WikiWhat</h4>
          <a href="LICENSE.txt"
            ><span class="small">Release under MIT License</span></a
          >
        </div>
      </div>

      <div class="container">
          <div class="card">
          <form method="POST" action="Profile.php">
              <a class="login">Log in</a>
              <div class="inputBox" class="">
                  <input type="text" required="required" name="username">
                  <span class="user">Username</span>
              </div>
     
              <!-- <div class="inputBox">
                  <input type="password" required="required">
                  <span>Password</span>
              </div> -->
     
              <!-- <button class="enter">Enter</button> -->
              <div class="circly">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <input type="submit" class="submit-text" name="Submit">
              </div>
          </form>
          <?php 
 
            // starting the session
            session_start();


            if (isset($_POST['Submit'])) { 
              $_SESSION['username'] = $_POST['username'];
            }
            if (isset($_SESSION['username'])) {              
              echo("Hello " . $_SESSION['username']);
            }
          ?>
          </div>
      </div>
    </div>
  </body>
</html>
